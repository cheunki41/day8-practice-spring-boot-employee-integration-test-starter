package com.afs.restapi.exception;

public class InvalidEmployeeException extends RuntimeException{

    public InvalidEmployeeException() {
        super("Employee must be 18~65 years old");
    }
}
