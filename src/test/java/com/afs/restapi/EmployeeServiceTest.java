package com.afs.restapi;

import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {
    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    EmployeeService employeeService;

    @Test
    public void should_throw_invalid_employee_exception_when_create_given_employee_less_than_18() {
        // given
        Employee employee = new Employee("Jackson", 17, "Male", 1000);

        // when
        Executable create = () -> employeeService.create(employee);

        // then
        String exceptionMessage = assertThrows(InvalidEmployeeException.class, create).getMessage();
        assertEquals("Employee must be 18~65 years old", exceptionMessage);
    }

    @Test
    public void should_throw_invalid_employee_exception_when_create_given_employee_larger_than_65() {
        // given
        Employee employee = new Employee("Marry", 66, "Female", 20);

        // when
        Executable create = () -> employeeService.create(employee);

        // then
        String exceptionMessage = assertThrows(InvalidEmployeeException.class, create).getMessage();
        assertEquals("Employee must be 18~65 years old", exceptionMessage);
    }

    @Test
    public void should_add_new_employee_when_create_given_employee_has_age_in_range_18_to_65() {
        // given
        Employee employee = new Employee("Tim", 45, "Male", 5000);

        // when
        employeeService.create(employee);

        // then
        verify(employeeRepository, times(1)).insert(employee);
    }

    @Test
    public void should_throw_inactive_employee_exception_when_update_given_inactive_employee_id() {
        // given
        Long id = 1L;
        Employee inactiveEmployee = new Employee(id, "Joyce", 20, "Female", 1000);
        inactiveEmployee.setActive(false);
        doReturn(inactiveEmployee).when(employeeRepository).findById(id);
        Employee updateData = new Employee();

        // when
        Executable update = () -> employeeService.updateEmployee(id, updateData);

        // then
        String exceptionMessage = assertThrows(InactiveEmployeeException.class, update).getMessage();
        assertEquals("Employee is inactive", exceptionMessage);
    }

    @Test
    public void should_get_all_employees_when_getAllEmployees_given_none() {
        // given
        // when
        employeeService.getAllEmployees();

        // then
        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    public void should_get_employee_by_id_when_getEmployeeById_given_id() {
        // given
        Long id = 1L;

        // when
        employeeService.getEmployeeById(id);

        // then
        verify(employeeRepository, times(1)).findById(id);
    }

    @Test
    public void should_find_employees_by_gender_when_findEmployeesByGender_given_gender() {
        // given
        String gender = "Female";

        // when
        employeeService.findEmployeesByGender(gender);

        // then
        verify(employeeRepository, times(1)).findByGender(gender);
    }

    @Test
    public void should_find_employees_by_page_when_findByPage_given_pageNumber_and_pageSize() {
        // given
        Integer pageNumber = 1;
        Integer pageSize = 3;

        // when
        employeeService.findByPage(pageNumber, pageSize);

        // then
        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    public void should_update_employee_when_updateEmployee_given_id_and_employee() {
        // given
        Long id = 1L;
        Employee updateData = new Employee();
        Employee activeEmployee = new Employee(id, "Joyce", 20, "Female", 1000);
        activeEmployee.setActive(true);
        doReturn(activeEmployee).when(employeeRepository).findById(id);

        // when
        employeeService.updateEmployee(id, updateData);

        // then
        verify(employeeRepository, times(1)).update(id, updateData);
    }

    @Test
    public void should_delete_employee_when_deleteEmployee_given_id() {
        // given
        Long id = 1L;

        // when
        employeeService.deleteEmployee(id);

        // then
        verify(employeeRepository, times(1)).delete(id);
    }


}
