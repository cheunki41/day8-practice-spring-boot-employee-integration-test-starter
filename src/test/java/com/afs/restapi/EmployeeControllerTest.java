package com.afs.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    MockMvc postmanMock;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    public void setup() {
        employeeRepository.reset();
    }

    @Test
    public void should_return_all_employees_when_getAllEmployees_given_employees() throws Exception {
        // given
        // when
        // then
        postmanMock.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    public void should_return_an_employee_when_getEmployeeById_given_employee_id() throws Exception {
        // given
        Long id = 1L;
        // when
        // then
        String uri = "/employees/" + id;
        postmanMock.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("John Smith"))
                .andExpect(jsonPath("$.age").value(32))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(5000));
    }

    @Test
    public void should_set_status_as_false_when_deleteEmployee_given_employee_id() throws Exception {
        // given
        Long id = 1L;
        // when
        // then
        String uri = "/employees/" + id;
        postmanMock.perform(MockMvcRequestBuilders.delete(uri))
                .andExpect(status().isNoContent());

        assertEquals(5, employeeRepository.findAll().size());
        assertFalse(employeeRepository.findById(id).getActive());
    }

    @Test
    public void should_return_employees_by_gender_when_findEmployeesByGender_given_gender() throws Exception {
        // given
        String gender = "Female";
        // when
        // then
        String uri = "/employees?gender=" + gender;
        postmanMock.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$..gender").value(everyItem(is("Female"))));
    }

    @Test
    public void should_update_employee_when_updateEmployee_given_employee_id_and_update_data() throws Exception {
        // given
        Long id = 1L;
        Employee employee = new Employee();
        employee.setAge(10);
        employee.setSalary(1000);

        // when
        // then
        String uri = "/employees/" + id;
        postmanMock.perform(MockMvcRequestBuilders.put(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employee)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(10))
                .andExpect(jsonPath("$.salary").value(1000));
    }

    @Test
    public void should_create_employee_when_insertEmployee_given_employee() throws Exception {
        // given
        Employee employee = new Employee("Susan", 20, "Female", 1000);

        // when
        // then
        String uri = "/employees";
        String result = postmanMock.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employee)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(is(notNullValue())))
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").value(20))
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").value(1000))
                .andExpect(jsonPath("$.active").value(true))
                .andReturn().getResponse().getContentAsString();

        assertEquals(6, employeeRepository.findAll().size());
        assertEquals(result, objectMapper.writeValueAsString(employeeRepository.findAll().get(5)));
    }

    @Test
    public void should_return_employees_by_page_when_findByPage_given_employees_pageNumber_and_pageSize() throws Exception {
        // given
        String pageNumber = "2";
        String pageSize = "3";

        // when
        // then
        List<Employee> actual = List.of(
                new Employee(4L, "Emily Brown", 23, "Female", 4500),
                new Employee(5L, "Michael Jones", 40, "Male", 7000)
        );

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("pageNumber", pageNumber)
                .param("pageSize", pageSize);
        postmanMock.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(result -> Objects.equals(result.getResponse().getContentAsString(), objectMapper.writeValueAsString(actual)));
    }

    @Test
    public void should_return_not_found_when_getEmployeeById_given_id() throws Exception {
        // given
        Long id = 999L;

        // when
        // then
        String uri = "/employees/" + id;
        postmanMock.perform(MockMvcRequestBuilders.get(uri))
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_return_not_found_when_updateEmployee_given_id() throws Exception {
        // given
        Long id = 999L;
        Employee employee = new Employee();
        employee.setSalary(20000);

        // when
        // then
        String uri = "/employees/" + id;
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employee));

        postmanMock.perform(requestBuilder)
                .andExpect(status().isNotFound());
    }

    @Test
    public void should_return_not_found_when_deleteEmployee_given_id() throws Exception {
        // given
        Long id = 999L;

        // when
        // then
        String uri = "/employees/" + id;
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete(uri);
        postmanMock.perform(requestBuilder)
                .andExpect(status().isNotFound());
    }


}
